package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"time"
)

func HttpDownload(url string, message chan string) {
	t := time.Now()
	resp, err := http.Get(url)

	if err != nil {
		message <- fmt.Sprint(err)
		return
	}

	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)

	if err != nil {
		message <- fmt.Sprint(err)
		return
	}

	message <- fmt.Sprintf("%.2fs\t%6d\t%s", time.Since(t).Seconds(), len(body), url)
}

func main() {
	start_time := time.Now()
	message := make(chan string)
	args := os.Args[1:]

	for _, url := range args {
		go HttpDownload(url, message)
	}

	for range args {
		fmt.Print(<-message + "\n")
	}

	fmt.Printf("%.2fs elapsed", time.Since(start_time).Seconds())
}
