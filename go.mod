module gitlab.com/TIoJIuHa/mai-practice-hw1

go 1.19

require (
	github.com/stretchr/testify v1.8.4
	gitlab.com/slon/shad-go v0.0.0-20230521151325-c3874bfe536c
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/kr/pretty v0.3.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/spf13/cobra v1.1.3 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/mod v0.9.0 // indirect
	golang.org/x/perf v0.0.0-20191209155426-36b577b0eb03 // indirect
	golang.org/x/sys v0.7.0 // indirect
	golang.org/x/tools v0.7.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
