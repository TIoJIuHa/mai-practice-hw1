package main

import (
	"bufio"
	"fmt"
	"os"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	args := os.Args[1:]
	count := make(map[string]int)

	for _, filename := range args {
		dat, err := os.Open(filename)
		check(err)

		fileScanner := bufio.NewScanner(dat)
		for fileScanner.Scan() {
			count[fileScanner.Text()] += 1
		}

		dat.Close()
	}

	for key, value := range count {
		if value > 1 {
			fmt.Printf("%d\t%s\n", value, key)
		}
	}
}
